package com.example.jackson.venture_forth;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationListener;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class Quest extends ActionBarActivity implements SensorEventListener {
    private ImageView image;
    private TextView longitude;
    private TextView latitude;
    private TextView accuracy;
    private TextView accuracy_label;
    private TextView hint;
    private TextView heading_text;
    private ImageView compass;
    private Button alpha_numeric_ask;
    private ProgressBar pending;
    private float currentDegree = 0f;
    private SensorManager sm;
    private LocationManager locationManager;
    TextView tHeading;

    private Sensor mAccelerometer;
    private Sensor mMagnetometer;

    private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];
    private boolean mLastAccelerometerSet = false;
    private boolean mLastMagnetometerSet = false;
    private float[] mR = new float[9];
    private float[] mOrientation = new float[3];

    static final float ALPHA = 0.25f;

    private double lat;
    private double lng;
    private double alti;

    float heading;

    private boolean show_longlat;
    private boolean show_accuracy;
    private boolean enable_proximity_alert;
    private boolean show_heading;

    private boolean bool_alert = false;

    private String provider;

    String current_hint;
    String current_alphanumeric;
    private int current_proximity;

    private int current_quest_index;
    private int current_destination_index;

    private QuestObject currentQuest;
    private QuestObject.Destination current_destination;
    private QuestList qL;

    private SharedPreferences sharedPreferences;


    Location current_destination_location = new Location ("dumpster string");


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        sharedPreferences = getSharedPreferences("vf_preferences", MODE_PRIVATE);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        /**
         * If the Extras on the Intent is not empty, get the passed current_quest_index and
         * current_destination_index, set the local variables to these, save the current Quest's
         * info, and set the current Quest to those items.
         */
        if(extras != null){
            current_quest_index = extras.getInt("current_quest_index");
            current_destination_index = extras.getInt("current_destination_index");

            Log.i("Fetched", "CDI: " + current_destination_index + " CQI: " + current_quest_index);

            saveCurrentQuestInfo();
            setCurrentDestination();
        }
        /**
         * If the Extras on the Intent is empty, then load the last Quest data.
         */
        else{
            loadLastQuest();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quest);


        /**
         * Make test Quest for testing purposes.
         */
        //QuestObject testQuest = new QuestObject("Test", "This is an example");
        //testQuest.addDestination(testLat, testLong, "It's a classroom!", "abcdefg", 50);

        bool_alert = false;



        /**
         * Load saved preferences.
         */
        loadSavedPreferences();

        /**
         * Assign view IDs.
         */

        image = (ImageView) findViewById(R.id.compass_pointer);
        longitude = (TextView) findViewById(R.id.longitude_textview);
        latitude = (TextView) findViewById(R.id.latitude_textview);
        accuracy = (TextView) findViewById(R.id.gps_accuracy);
        accuracy_label = (TextView) findViewById(R.id.gps_accuracy_label);
        heading_text = (TextView) findViewById(R.id.heading);
        alpha_numeric_ask = (Button) findViewById(R.id.alphanumeric_ask_button);

        /**
         * Set visibility of longitude, latitude, and accuracy textfields based on
         * saved user-preferences.
         */

        /**
         * Assign more view IDs.
         */
        tHeading = (TextView) findViewById(R.id.heading);
        hint = (TextView) findViewById(R.id.quest_hintview);
        pending = (ProgressBar) findViewById(R.id.progressBar);

        sm = (SensorManager) getSystemService(SENSOR_SERVICE);

        mAccelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = sm.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(!gpsEnabled){

            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.gps_not_enabled))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent GPSintent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(GPSintent);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }


        /**
         * Assign Criteria to be used for sensing location.
         * Fine accuracy is not required, but altitude and bearing are needed to accurately
         * determine user's heading to second location from current location.
         */
        Criteria criteria = new Criteria();
        //criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(true);
        criteria.setBearingRequired(true);
        criteria.setCostAllowed(true);

        /**
         * Assign a new location manager and get user's current location.
         */

        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);

        /**
         * If the location found is not null, set values obtained from location data including
         * Longitude, Latitude, and Altitude. These values are then assigned to their
         * corresponding TextViews.
         */
        if(location != null)
        {
            lat = (double) (location.getLatitude());
            lng = (double) (location.getLongitude());
            alti = (double) (location.getAltitude());

            latitude.setText(getString(R.string.latitude_label) + " " + String.valueOf(lat));
            longitude.setText(getString(R.string.longitude_label) + " " + String.valueOf(lng));
        }

        /**
         * If the location found is null (that is, no location data could be obtained) the user
         * is prompted with an alert saying the signal could not be found and advises them to try
         * again outdoors.
         */

        else
        {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.gps_signal_not_found),
                    Toast.LENGTH_LONG).show();
        }

        alpha_numeric_ask.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume(){
        super.onResume();

        sm.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
        sm.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_GAME);
        locationManager.requestLocationUpdates(provider, 400, 1, locationListener);

        loadSavedPreferences();
        setVisibilityOfViews();
        loadLastQuest();

        setCurrentDestination();
    }

    @Override
    public void onPause(){
        saveCurrentQuestInfo();

        super.onPause();
        sm.unregisterListener(this, mAccelerometer);
        sm.unregisterListener(this, mMagnetometer);
        locationManager.removeUpdates(locationListener);
    }

    @Override
    public void onSensorChanged(SensorEvent event){
        //Log.i("SensorEvent", "Sensor Changed");
        if(event.sensor == mAccelerometer)
        {
            System.arraycopy(event.values, 0, mLastAccelerometer, 0, event.values.length);
            mLastAccelerometerSet = true;
        }
        else if(event.sensor == mMagnetometer)
        {
            System.arraycopy(event.values, 0, mLastMagnetometer, 0, event.values.length);
            mLastMagnetometerSet = true;
        }
        if(mLastAccelerometerSet && mLastAccelerometerSet)
        {
            SensorManager.getRotationMatrix(mR, null, mLastAccelerometer, mLastMagnetometer);
            SensorManager.getOrientation(mR, mOrientation);
            float azimuthInRadians = mOrientation[0];
            float azimuthInDegrees = (float)(Math.toDegrees(azimuthInRadians)+360)%360;


            /**
             * Store latitude, longitude, and altitude as floats to pass them to a geoField.
             */

            float geoLat = (float) lat;
            float geoLng = (float) lng;
            float geoAlti = (float) alti;

            /**
             * Create new GeomagneticField
             */
            GeomagneticField geoField = new GeomagneticField(
                    geoLat,
                    geoLng,
                    geoAlti,
                    System.currentTimeMillis()
            );

            /**
             * Find actual heading taking azimuth into account.
             */

            azimuthInDegrees += geoField.getDeclination();
            azimuthInDegrees = azimuthInDegrees - heading;

            //Log.i("Azimuth", "" + azimuthInDegrees);

            /**
             * Rotate pointer based on heading.
             */
            RotateAnimation ra = new RotateAnimation(
                    currentDegree,
                    -azimuthInDegrees,
                    Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF, 0.5f);

            ra.setDuration(1000);
            ra.setFillAfter(true);

            /**
             * Set text for the hint label.
             */

            hint.setText(getString(R.string.hint_label) + " " + current_hint);

            if(show_heading){
                heading_text.setText("Heading: " + heading);
            }
            if(image.isShown())
            {
                image.startAnimation(ra);
            }
            currentDegree = -azimuthInDegrees;
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy){

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quest, menu);
        return true;
    }

    /**
     * Go to preferences menu.
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, Preferences.class);
            startActivity(settingsIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method to display Toast notifications/error messages.
     * @param c   Message to be displayed.
     * @param duration     Long or Short duration of message.
     */
    public void toastTheToast(CharSequence c, int duration) {
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, c, duration);
        toast.show();
    }

    /**
     * Assign locationlistener.
     */
    private final LocationListener locationListener = new LocationListener()
    {
        public void onLocationChanged(Location l)
        {
            //Log.i("Location", "Location Changed");
            image.setVisibility(View.VISIBLE);
            pending.setVisibility(View.INVISIBLE);

            float lat = (float) (l.getLatitude());
            float lng = (float) (l.getLongitude());
            float acu = (l.getAccuracy());

            latitude.setText(getString(R.string.latitude_label) + " " + String.valueOf(lat));
            longitude.setText(getString(R.string.longitude_label) + " " + String.valueOf(lng));
            accuracy.setText(String.valueOf(acu));


            /**
             * Get bearing to test location.
             */
            heading = l.bearingTo(current_destination_location);

            //Log.i("Heading", "" + heading);

            /**
             * If the user is currenty at or closer than the destination's set proximity, the option
             * to enter in the destination's alphanumeric code is then given, along with a proximity
             * alert if the user has that option enabled.
             */
            if(l.distanceTo(current_destination_location) <= current_proximity){
                alpha_numeric_ask.setVisibility(View.VISIBLE);
                if(enable_proximity_alert && (!bool_alert)){
                    proximityAlert();
                }
            }
        }
        public void onProviderDisabled(String provider)
        {

        }
        public void onProviderEnabled(String provider)
        {

        }
        public void onStatusChanged(String provider, int status, Bundle extras)
        {

        }
    };

    /**
     * Loads saved user preferences.
     */
    private void loadSavedPreferences(){

        Log.i("Preferences", "Loading saved preferences...");

        SharedPreferences sharedPreferences = getSharedPreferences("vf_Preferences", MODE_PRIVATE);

        show_longlat = sharedPreferences.getBoolean("show_longlat", false);
        show_accuracy = sharedPreferences.getBoolean("show_accuracy", false);
        enable_proximity_alert = sharedPreferences.getBoolean("enable_proximity_alert", true);
        show_heading = sharedPreferences.getBoolean("show_heading", false);




    }

    /**
     * Save the index of the current quest and current destination within that quest.
     */
    private void saveCurrentQuestInfo(){
        Log.i("Saving", "Saving current quest info...");

        SharedPreferences sharedPreferences = getSharedPreferences("vf_Preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt("current_destination_index", current_destination_index);
        editor.putInt("current_quest_index", current_quest_index);
        editor.commit();

        Log.i("CDInfo Saving", "" + current_destination_index + "   " + current_quest_index);
    }

    /**
     * Sets visibility of certain views per user-preferences.
     */
    private void setVisibilityOfViews(){
        if(show_longlat){
            longitude.setVisibility(View.VISIBLE);
            latitude.setVisibility(View.VISIBLE);
        }
        else{
            longitude.setVisibility(View.INVISIBLE);
            latitude.setVisibility(View.INVISIBLE);
        }

        if(show_accuracy){
            accuracy.setVisibility(View.VISIBLE);
            accuracy_label.setVisibility(View.VISIBLE);

        }
        else{
            accuracy.setVisibility(View.INVISIBLE);
            accuracy_label.setVisibility(View.INVISIBLE);
        }
        if(show_heading){
            heading_text.setVisibility(View.VISIBLE);
            //Log.i("HeadingView", "Should be visible.");
        }
        else{
            heading_text.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Method to create alert tone when user is within the set proximity range.
     */
    private void proximityAlert(){
        try{
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone rTone = RingtoneManager.getRingtone(getApplicationContext(), notification);
            rTone.play();
            bool_alert = true;

        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Handles the buttonpress of the alphanumeric button.
     * Pop-up editText dialog box.
     * @param v
     */
    public void alphaNumericButton(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View view2 = inflater.inflate(R.layout.alphanumeric_entry_dialog, null);

        builder.setView(view2)
                //Add buttons
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // assign text
                        EditText temp;
                        temp = (EditText) view2.findViewById(R.id.alphanumeric_user_input);
                        String alpha_test = temp.getText().toString();
                        //showAlphaTest(alpha_test);
                        testAlpha(alpha_test);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    /**
     * Test the alphanumeric string inputted by the user (once they are within proximity range of the
     * destination) against the actual alphanumeric string as stored by the destination.
     * If it matches the user is congratulated and the next destination is set.
     * If it does not match the user is prompted to try again.
     * @param s
     */
    public void testAlpha(String s){
        LayoutInflater inflater = getLayoutInflater();
        View toaster = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_layout_root));
        TextView toastText = (TextView) toaster.findViewById(R.id.toast_text);

        if(current_alphanumeric.equalsIgnoreCase(s)){

            Toast piece_of_toast = new Toast(getApplicationContext());
            toastText.setText(getString(R.string.alphanumeric_matches));
            piece_of_toast.setGravity(Gravity.CENTER, 0, 0);
            piece_of_toast.setDuration(Toast.LENGTH_LONG);
            piece_of_toast.setView(toaster);
            piece_of_toast.show();
            //alpha_numeric_ask.setVisibility(View.INVISIBLE);

            //Go to next item in QuestObject list (Next Destination).

            //if(currentQuest.hasNext(current_destination_index)){
            if(current_destination_index < (currentQuest.getQuestLength() - 1)){
                //currentQuest.setNext();
                ++current_destination_index;
                saveCurrentQuestInfo();
                setCurrentDestination();
            }

            else{
                questComplete();
            }

        }
        else{
            //Alert user to try again.
            Toast piece_of_toast = new Toast(getApplicationContext());
            toastText.setText(getString(R.string.alphanumeric_not_equal));
            piece_of_toast.setGravity(Gravity.CENTER, 0, 0);
            piece_of_toast.setDuration(Toast.LENGTH_LONG);
            piece_of_toast.setView(toaster);
            piece_of_toast.show();
        }
    }
/*
    protected float[] lowPassFilter(float[] inputData, float[] outputData){

        if ( outputData == null ) return inputData;
        for ( int i=0; i<inputData.length; i++ ) {
            outputData[i] = outputData[i] + ALPHA * (inputData[i] - outputData[i]);
        }
        return outputData;
    }
*/
    /**
     * Refresh the currentDestination items.
     */
    public void setCurrentDestination(){
        qL = new QuestList(this);
        currentQuest = qL.getQuest(current_quest_index);
        current_destination = currentQuest.getDestination(current_destination_index);
        current_destination_location = current_destination.getLocation();
        current_hint = current_destination.getHint();
        Log.i("Hint", current_hint);
        current_alphanumeric = current_destination.getAlphanumeric();
        current_proximity = current_destination.getProximity();
    }

    /**
     * Load the last quest the user was currently following if available.
     */
    private void loadLastQuest(){

        SharedPreferences sharedPreferences = getSharedPreferences("vf_Preferences", MODE_PRIVATE);
        current_destination_index = sharedPreferences.getInt("current_destination_index", 0);
        current_quest_index = sharedPreferences.getInt("current_quest_index", 0);

        Log.i("CDInfo Loading", "" + current_destination_index + "   " + current_quest_index);
    }

    /**
     * Method that runs upon quest completion: When the index counter excedes the size of the
     * current quest - 1.
     */
    public void questComplete(){
        toastTheToast(getString(R.string.quest_completed_text), Toast.LENGTH_LONG);
        current_destination_index = 0;
        current_quest_index = 0;
        saveCurrentQuestInfo();
    }
}