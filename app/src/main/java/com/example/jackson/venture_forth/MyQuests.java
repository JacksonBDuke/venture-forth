package com.example.jackson.venture_forth;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class MyQuests extends ActionBarActivity {

    private SharedPreferences sharedPreferences;

    Context context = this;

    QuestList qL;
    ArrayList<QuestObject> questArrayList = new ArrayList();
    ArrayList<String> listOfQuestNames = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();

        qL = new QuestList(context);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_quests);

        questArrayList = qL.getList();
        listOfQuestNames = qL.getListOfQuestNames();
        showQuests();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_quests, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings){
            Intent settingsIntent = new Intent(this, Preferences.class);
            startActivity(settingsIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addQuest(String name, String obj){
        qL.addQuest(name, obj);
    }


    /**
     * Method called to display/refresh list of Quests.
     */
    public void showQuests(){
        ListView questList = (ListView)findViewById(R.id.my_quests_list);

        QuestAdapter adapter = new QuestAdapter(this, listOfQuestNames);

        Log.i("listOfQuestNames", "" + listOfQuestNames.size());

        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, listOfQuestNames);

        questList.setAdapter(adapter);


    }

    public void onClickAdd(View v){

        sharedPreferences = getSharedPreferences("vf_preferences", MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();

        Intent intent = new Intent(this, Quest.class);
        intent.putExtra("current_quest_index", 0);
        intent.putExtra("current_destination_index", 0);

        edit.putInt("current_quest_index", 0);
        edit.putInt("current_destination_index", 0);
        edit.commit();

        startActivity(intent);

    }

    public void onClickQuest(View v){
        // Get listView's position.
        // Make questObject for that position.
        // Get string values for those.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View questEntryView = inflater.inflate(R.layout.edit_start_quest_dialog, null);

        TextView selectedQuestName = (TextView)findViewById(R.id.edit_start_quest_name_text);
        TextView selectedQuestObjective = (TextView)findViewById(R.id.edit_start_quest_objective_text);

        builder.setView(questEntryView)
                //Add buttons
                .setPositiveButton(getString(R.string.start_quest_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Start quest.
                        dialog.dismiss();

                    }
                })
                .setNegativeButton(getString(R.string.edit_quest_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Edit quest view.
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }
}
