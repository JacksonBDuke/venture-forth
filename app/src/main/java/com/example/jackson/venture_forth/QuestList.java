package com.example.jackson.venture_forth;

import android.content.Context;
import android.os.Parcel;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Jackson on 4/28/2015.
 */
public class QuestList {
    private String filePath = "QuestList.data";
    private ArrayList<QuestObject> listOfQuests = new ArrayList<QuestObject>();
    private ArrayList<String> listOfQuestNames = new ArrayList<String>();

    Context context;

    public QuestList(Context c){
        context = c;
        File f = new File(c.getFilesDir(), filePath);
        if(!f.exists()){
        initializeFile();
        }
        try{
            loadQuests();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public ArrayList<QuestObject> getList(){
        return listOfQuests;
    }

    public void loadQuests() throws IOException {
        ObjectInputStream oIS = null;
        File inputFile = null;

        try{
            inputFile = new File(context.getFilesDir(), filePath);
            FileInputStream fIS = new FileInputStream(inputFile);
            oIS = new ObjectInputStream(fIS);

            listOfQuests = (ArrayList<QuestObject>)oIS.readObject();

            oIS.close();
            fIS.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        for(int i = 0; i <= (listOfQuests.size() - 1); ++i){
            listOfQuestNames.add(listOfQuests.get(i).getQuestName());
        }
    }

    public void saveQuests(ArrayList<QuestObject> tList) throws IOException{
        ObjectOutputStream oOS = null;
        File outputFile = null;

         ArrayList<QuestObject> tempList = tList;


        try{
            outputFile = new File(context.getFilesDir(), filePath);
            FileOutputStream fOS = new FileOutputStream(outputFile);
            oOS = new ObjectOutputStream(fOS);

            Log.i("Save", "Calling writeObject");

            oOS.writeObject(tempList);

            oOS.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void initializeFile(){
        ArrayList<QuestObject> initialList = new ArrayList();
        QuestObject qTemp = new QuestObject("My daily commute.", "An example quest.");
        qTemp.addDestination(32.745261, -97.328115, "It's a train station.", "12345", 50);
        qTemp.addDestination(32.775941, -96.808200, "Another train station.", "12345", 50);
        qTemp.addDestination(33.003290, -96.703176, "Catch a bus!", "abcdefg", 50);
        qTemp.addDestination(32.988160, -96.749191, "Get to class on time!", "hijk", 50);

        initialList.add(qTemp);
        //listOfQuests.add(qTemp);

        try{
            Log.i("Save", "Trying to save...");
            saveQuests(initialList);
        }
        catch(Exception e){
            e.printStackTrace();
        }


    }

    public void deleteQuest(int i){
        listOfQuests.remove(i);
    }

    public void addQuest(String name, String obj){
        QuestObject tempQuest = new QuestObject(name, obj);
        listOfQuests.add(tempQuest);
    }

    public QuestObject getQuest(int i){
        return listOfQuests.get(i);
    }

    public ArrayList<String> getListOfQuestNames(){
        return listOfQuestNames;
    }
}
