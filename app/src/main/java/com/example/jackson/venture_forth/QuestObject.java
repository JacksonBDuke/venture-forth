package com.example.jackson.venture_forth;

import android.location.Location;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Jackson on 4/27/2015.
 *
 * Class representing an individual Quest.
 * Each Quest holds multiple 'Destinations'
 */
public class QuestObject implements Serializable{

    private String quest_name;
    private String objective;
    private ArrayList<Destination> destinationList = new ArrayList<Destination>();
    private int currentDestinationIndex;

    /**
     * Default empty consttructor.
     */
    public QuestObject(){
    }

    /**
     * Constructor to take the name and objective of a created Quest.
     * @param nm
     * @param obj
     */
    public QuestObject(String nm, String obj){
        quest_name = nm;
        objective = obj;
        currentDestinationIndex = 0;
    }

    /**
     * Adds a new Destination object to the list of Destinations in the Quest.
     * @param lat
     * @param lng
     * @param hnt
     * @param alpha
     * @param prox
     */
    public void addDestination(double lat, double lng, String hnt, String alpha, int prox){
        Destination tempDest = new Destination(lat, lng, hnt, alpha, prox);
        destinationList.add(tempDest);
    }

    /**
     * Get the number of Destinations in the Quest.
     * @return
     */
    public int getQuestLength(){
        return destinationList.size();
    }

    /**
     * Get an individual Destination in the current Quest.
     * @param i
     * @return
     */
    public Destination getDestination(int i){
        return destinationList.get(i);
    }

    public String getQuestName(){
        return quest_name;
    }
    public String getObjective(){
        return objective;
    }

    /**
     * Takes
     * @return
     */
    /*
    public boolean hasNext(){
        //String diag = "cdi: " + currentDestinationIndex + "   dlS: " + (destinationList.size() - 1);
        // Log.i("hasNext", diag);
        return (currentDestinationIndex < (destinationList.size() - 1));
        //return (cd < (destinationList.size() - 1));

    }

    public void setNext(){
        if(hasNext()){
            currentDestinationIndex++;
        }
    }
    public int getCurrentDestinationIndex(){
        return currentDestinationIndex;
    }
*/
    /**
     * New class to hold data pertaining to each location.
     * Longitude, Latitude, Destination Hint, Destination Alphanumeric Code, and the Destination's
     * Proximity make up a Destination.
     */
    public class Destination implements Serializable{

        private double longitude;
        private double latitude;
        private String hint;
        private String alphanumeric;
        private int proximity;

        Destination(double lat, double lng, String hnt, String alpha, int prox){
            latitude = lat;
            longitude = lng;
            hint = hnt;
            alphanumeric = alpha;
            proximity = prox;
        }

        public int getProximity(){
            return proximity;
        }
        public double getLatitude(){
            return latitude;
        }
        public double getLongitude(){
            return longitude;
        }
        public String getHint(){
            return hint;
        }
        public String getAlphanumeric(){
            return alphanumeric;
        }

        /**
         * Creates and returnsa Location object from the stored Destination's Longitude and
         * Latitude.
         * @return
         */
        public Location getLocation(){
            Location destLoc = new Location("dumpster");

            destLoc.setLatitude(latitude);
            destLoc.setLongitude(longitude);

            return destLoc;
        }
    }

}
