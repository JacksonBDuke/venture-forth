package com.example.jackson.venture_forth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.io.File;


public class MainActivity extends ActionBarActivity {

    boolean first_run;
    //File f = new File("QuestList.data");

    //getApplicationContext();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("vf_Preferences", MODE_PRIVATE);
        first_run = sharedPreferences.getBoolean("first_run", true);

        setVisibility();

        /**
         * Checks to see if file used to save/load list of Quests exists. If it does not exist, this
         * is the application's first run.
         *
         * If it is the application's first run, the Continue button is hidden since there is no
         * current Quest being followed.
         *
         * Note: Change this to a better implementation of a SharedPreferences boolean.
         */
        File f = new File(((Context)this).getFilesDir(), "QuestList.data");
        if(first_run || (!f.exists())){
            Log.i("FileTest", "File doesn't exist. First run.");
            initializeApplication();
        }
        else{
            Log.i("FileTest", "File exists.");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings){
            Intent settingsIntent = new Intent(this, Preferences.class);
            startActivity(settingsIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * If the Continue button is clickied, go directly to Quest activity.
     * @param view
     */
    public void onClickContinue(View view){
        Intent intent = new Intent(this, Quest.class);
        startActivity(intent);
    }

    /**
     * If MyQuests button is clicked, go to the MyQuests activity.
     * @param view
     */
    public void onClickMyQuests(View view){
        Intent intent = new Intent(this, MyQuests.class);
        startActivity(intent);
    }

    /**
     * Called if it's the application's first run. Toggles the SharedPreferences boolean that is
     * related to the application's first run, and hides the Continue button.
     */
    private void initializeApplication(){
        SharedPreferences sharedPreferences = getSharedPreferences("vf_Preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("first_run", false);
        editor.commit();

        Button continueButton = (Button) findViewById(R.id.home_continue_button);
        continueButton.setVisibility(View.GONE);
    }

    private void setVisibility(){
        Button continueButton = (Button) findViewById(R.id.home_continue_button);
        continueButton.setVisibility(View.VISIBLE);
    }
}
