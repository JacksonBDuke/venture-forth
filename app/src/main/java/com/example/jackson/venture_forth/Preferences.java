package com.example.jackson.venture_forth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;


public class Preferences extends ActionBarActivity {

    private boolean show_longlat;
    private boolean show_accuracy;
    private boolean enable_proximity_alert;
    private boolean show_heading;

    private CheckBox CB_longlat;
    private CheckBox CB_accuracy;
    private CheckBox CB_proximity;
    private CheckBox CB_heading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        CB_longlat = (CheckBox) findViewById(R.id.preferences_show_longlat_button);
        CB_accuracy = (CheckBox) findViewById(R.id.preferences_show_accuracy_button);
        CB_proximity = (CheckBox) findViewById(R.id.preferences_enable_proximity_alert_button);
        CB_heading = (CheckBox) findViewById(R.id.preferences_show_heading_button);

        /////
        loadSavedPreferences();
        /////

        CB_longlat.setChecked(show_longlat);
        CB_accuracy.setChecked(show_accuracy);
        CB_proximity.setChecked(enable_proximity_alert);
        CB_heading.setChecked(show_heading);
    }

    @Override
    protected void onPause(){
        super.onPause();
        //Save settings.
        savePreferences("show_longlat", CB_longlat.isChecked());
        savePreferences("show_accuracy",CB_accuracy.isChecked());
        savePreferences("enable_proximity_alert",CB_proximity.isChecked());
        savePreferences("show_heading",CB_heading.isChecked());
        Log.i("Heading: ", "" + show_heading);
    }

    private void loadSavedPreferences(){
        SharedPreferences sharedPreferences = getSharedPreferences("vf_Preferences", MODE_PRIVATE);

        show_longlat = sharedPreferences.getBoolean("show_longlat", false);
        show_accuracy = sharedPreferences.getBoolean("show_accuracy", false);
        enable_proximity_alert = sharedPreferences.getBoolean("enable_proximity_alert", true);
        show_heading = sharedPreferences.getBoolean("show_heading", false);
    }

    private void savePreferences(String key, boolean value){
        SharedPreferences sharedPreferences = getSharedPreferences("vf_Preferences", MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void clickLongLat(View v){
        show_longlat = !show_longlat;
        CB_longlat.setChecked(show_longlat);
    }
    public void clickLongLatCB(View v){
        show_longlat = !show_longlat;
    }

    public void clickAccuracy(View v){
        show_accuracy = !show_accuracy;
        CB_accuracy.setChecked(show_accuracy);
    }
    public void clickAccuracyCB(View v){
        show_accuracy = !show_accuracy;
    }

    public void clickProximity(View v){
        enable_proximity_alert = !enable_proximity_alert;
        CB_proximity.setChecked(enable_proximity_alert);
    }
    public void clickProximityCB(View v){
        enable_proximity_alert=!enable_proximity_alert;
    }

    public void clickHeading(View v){
        show_heading = !show_heading;
        CB_heading.setChecked(show_heading);
    }
    public void clickHeadingCB(View v){
        show_heading = !show_heading;
    }
}
