package com.example.jackson.venture_forth;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import java.util.ArrayList;

/**
 * Created by Jackson on 4/29/2015.
 *
 * Custom ArrayAdapter to show list of Quests in the MyQuests activity.
 * Note: Still need to implement onClick for each Quest item individually.
 */
public class QuestAdapter extends ArrayAdapter<String>{

    public QuestAdapter(Context context, ArrayList<String> questNames){
        super(context, 0, questNames);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        String quest = getItem(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_quest, parent, false);
        }

        Button questName = (Button) convertView.findViewById(R.id.item_quest_name);

        questName.setText(quest);

        return convertView;
    }
}
